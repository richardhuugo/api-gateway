import { join } from "path";

import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { GraphQLModule, GqlModuleOptions } from "@nestjs/graphql";
import { LoggerModule, PinoLogger } from "nestjs-pino";

import {
  DateTimeResolver,
  EmailAddressResolver,
  UnsignedIntResolver,
} from "graphql-scalars";
import { GraphQLJSONObject } from "graphql-type-json";

import { SearchModule } from "./elasticsearch/search.module";
import { playgroundQuery } from "./graphql/playground-query";
import { TipoDocumentosModule } from "./services/tipo-documentos/tipo-documentos.module";
import { EmpresasModule } from "./services/empresas/empresas.module";
import { DocumentosModule } from "./services/documentos/documentos.module";
import { ClientesModule } from "./services/clientes/clientes.module";
import { AuthModule } from "./auth/auth.module";
import { ResponsaveisModule } from "./services/responsaveis/responsaveis.module";

@Module({
  imports: [
    ConfigModule.forRoot(),
    GraphQLModule.forRootAsync({
      imports: [],
      useFactory: async (): Promise<GqlModuleOptions> => ({
        path: "/",
        subscriptions: "/",
        typePaths: ["./**/*.graphql"],
        resolvers: {
          DateTime: DateTimeResolver,
          EmailAddress: EmailAddressResolver,
          UnsignedInt: UnsignedIntResolver,
          JSONObject: GraphQLJSONObject,
        },
        definitions: {
          path: join(__dirname, "graphql.ts"),
        },
        debug: true,
        cors: false,
        installSubscriptionHandlers: true,
        playground: {
          endpoint: "/",
          subscriptionEndpoint: "/",
          settings: {
            "request.credentials": "include",
          },
          tabs: [
            {
              name: "GraphQL API",
              endpoint: "/",
              query: playgroundQuery,
            },
          ],
        },
        context: ({ req, res }): any => ({ req, res }),
      }),
      inject: [],
    }),

    ClientesModule,
    EmpresasModule,
    TipoDocumentosModule,
    DocumentosModule,
    ResponsaveisModule,
    SearchModule,
    AuthModule,
  ],
  exports: [ClientesModule],
})
export class AppModule {}
