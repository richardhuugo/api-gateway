
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface ResponsavelSignup {
    nome_completo: string;
    email: string;
    celular: string;
    password: string;
    empresa_id?: string;
}

export interface ResponsavelSignin {
    email: string;
    password: string;
}

export interface UsuarioInput {
    nome_completo: string;
    dt_nascimento: string;
    sexo: string;
    email?: string;
    celular?: string;
    telefone?: string;
    documento?: string;
    tipo_documento_id: string;
}

export interface DocumentoInput {
    nr_documento: string;
    tipo_documento_id: string;
    user_id?: string;
}

export interface EmpresaInput {
    nm_fantasia: string;
    razao_social: string;
    email?: string;
    telefone?: string;
    documento?: string;
    tipo_documento_id: string;
}

export interface EspecialidadesInput {
    especialidade: string;
}

export interface ResponsaveisInput {
    nome_completo: string;
    email: string;
    celular: string;
    password: string;
    empresa_id?: string;
}

export interface TipoDocumentoInput {
    tipo: string;
}

export interface IMutation {
    createTipoDocumento(data: TipoDocumentoInput): TipoDocumentosPayload | Promise<TipoDocumentosPayload>;
    createDocumento(data: DocumentoInput): DocumentosPayload | Promise<DocumentosPayload>;
    createUsuario(data: UsuarioInput): ClientesPayload | Promise<ClientesPayload>;
    createEmpresa(data?: EmpresaInput): EmpresasPayload | Promise<EmpresasPayload>;
    cadastro(data?: ResponsavelSignup): AuthPayload | Promise<AuthPayload>;
    login(data?: ResponsavelSignin): AuthPayload | Promise<AuthPayload>;
    refreshToken(): AuthPayload | Promise<AuthPayload>;
    logout(): boolean | Promise<boolean>;
}

export interface IQuery {
    tipoDocumentos(q?: string, first?: number, page?: number, last?: number, before?: string, after?: string, filterBy?: JSONObject, orderBy?: string): TipoDocumentosConnection | Promise<TipoDocumentosConnection>;
    clientes(q?: string, first?: number, last?: number, before?: string, after?: string, filterBy?: JSONObject, orderBy?: string): ClientesConnection | Promise<ClientesConnection>;
    empresas(q?: string, first?: number, last?: number, before?: string, after?: string, filterBy?: JSONObject, orderBy?: string): EmpresasConnection | Promise<EmpresasConnection>;
    especialidades(): Especialidades[] | Promise<Especialidades[]>;
}

export interface AuthPayload {
    errors?: ErrorPayload[];
    accessToken?: string;
    refreshToken?: string;
    responsavel?: Responsaveis;
}

export interface Clientes {
    id: string;
    nome_completo: string;
    dt_nascimento: string;
    email?: string;
    celular?: string;
    telefone?: string;
    empresa_id?: string;
    documento?: Documentos;
}

export interface ClientesConnection {
    edges: ClientesEdge[];
    pageInfo: PageInfo;
}

export interface ClientesEdge {
    node: Clientes;
    cursor: string;
}

export interface ClientesPayload {
    errors?: ErrorPayload[];
    cliente?: Clientes;
}

export interface ErrorPayload {
    field?: string;
    message?: string[];
}

export interface PageInfo {
    startCursor: string;
    endCursor: string;
    hasNextPage: boolean;
    hasPreviousPage: boolean;
}

export interface Documentos {
    id: string;
    nr_documento: string;
    tipo_documento_id: string;
    user_id: string;
}

export interface DocumentosConnection {
    edges: DocumentosEdge[];
    pageInfo: PageInfo;
}

export interface DocumentosEdge {
    node: Documentos;
    cursor: string;
}

export interface DocumentosPayload {
    errors?: ErrorPayload[];
    documento?: Documentos;
}

export interface Empresas {
    id: string;
    nm_fantasia: string;
    razao_social: string;
    email: string;
    telefone: string;
    documento_id: string;
    hash_integration?: string;
    documento?: Documentos;
}

export interface EmpresasConnection {
    edges: EmpresasEdge[];
    pageInfo: PageInfo;
}

export interface EmpresasEdge {
    node: Empresas;
    cursor: string;
}

export interface EmpresasPayload {
    errors?: ErrorPayload[];
    empresa?: Empresas;
}

export interface Especialidades {
    id: string;
    especialidade: string;
}

export interface EspecialidadesConnection {
    edges: EspecialidadesEdge[];
    pageInfo: PageInfo;
}

export interface EspecialidadesEdge {
    node: Especialidades;
    cursor: string;
}

export interface EspecialidadesPayload {
    errors?: ErrorPayload[];
    especialidade?: Especialidades;
}

export interface Responsaveis {
    id: string;
    nome_completo: string;
    email: string;
    celular: string;
    empresa_id: string;
}

export interface ResponsaveisConnection {
    edges: ResponsaveisEdge[];
    pageInfo: PageInfo;
}

export interface ResponsaveisEdge {
    node: Responsaveis;
    cursor: string;
}

export interface ResponsaveisPayload {
    errors?: ErrorPayload[];
    responsavel?: Responsaveis;
}

export interface TipoDocumentos {
    id: string;
    tipo: string;
}

export interface TipoDocumentosConnection {
    edges: TipoDocumentosEdge[];
    pageInfo: PageInfo;
}

export interface TipoDocumentosEdge {
    node: TipoDocumentos;
    cursor: string;
}

export interface TipoDocumentosPayload {
    errors?: ErrorPayload[];
    tipoDocumento?: TipoDocumentos;
}

export type DateTime = any;
export type EmailAddress = any;
export type UnsignedInt = any;
export type JSONObject = any;
