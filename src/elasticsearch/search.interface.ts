export interface SearchPayload {
  index: string;
  context: object;
}

export interface SearchResult {
  hits: {
    total: number;
    hits: Array<{
      _source: SearchPayload;
    }>;
  };
}
