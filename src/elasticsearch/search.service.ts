import { Injectable } from "@nestjs/common";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { SearchPayload, SearchResult } from "./search.interface";

@Injectable()
export default class SearchService {
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async indexSearch(search: SearchPayload) {
    return this.elasticsearchService.index<SearchResult>({
      index: search.index,
      body: search.context,
    });
  }

  async search(text: string, index: string, fields) {
    const { body } = await this.elasticsearchService.search<SearchResult>({
      index,
      body: {
        query: {
          multi_match: {
            query: text,
            fields,
          },
        },
      },
    });
    const hits = body.hits.hits;
    return hits.map((item) => item._source);
  }
}
