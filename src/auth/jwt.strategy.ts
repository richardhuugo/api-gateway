import { PassportStrategy } from "@nestjs/passport";
import { ConfigService } from "@nestjs/config";
import { Injectable, OnModuleInit, Inject } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { get } from "lodash";
import { Strategy, ExtractJwt } from "passport-jwt";
import { IResponsaveisService } from "src/services/responsaveis/responsaveis.interface";
import { Responsaveis } from "src/graphql/typings";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "jwt")
  implements OnModuleInit {
  constructor(
    @Inject("ResponsaveisServiceClient")
    private readonly responsaveisServiceClient: ClientGrpcProxy,

    private readonly configService: ConfigService
  ) {
    super({
      secretOrKey: configService.get<string>("JWT_ACCESSTOKEN_SECRET"),
      issuer: configService.get<string>("JWT_ISSUER"),
      audience: configService.get<string>("JWT_AUDIENCE"),
      jwtFromRequest: ExtractJwt.fromExtractors([
        (req) => get(req, "cookies.access-token"),
      ]),
    });
  }

  private responsaveisService: IResponsaveisService;

  onModuleInit(): void {
    this.responsaveisService = this.responsaveisServiceClient.getService<
      IResponsaveisService
    >("ResponsavelsService");
  }

  async validate(payload: any): Promise<Responsaveis> {
    return this.responsaveisService
      .findById({
        id: payload.sub,
      })
      .toPromise();
  }
}
