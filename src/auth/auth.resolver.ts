import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Resolver, Args, Mutation, Context } from "@nestjs/graphql";

import { isEmpty } from "lodash";

import { AuthService } from "./auth.service";
import { RefreshAuthGuard } from "./refresh-auth.guard";
import { CurrentUser } from "./user.decorator";

import {
  ResponsavelSignup,
  ResponsaveisPayload,
  ResponsavelSignin,
  Responsaveis,
  AuthPayload,
} from "../graphql/typings";

import { PasswordUtils } from "../utils/password.utils";
import { IResponsaveisService } from "src/services/responsaveis/responsaveis.interface";

@Resolver()
export class AuthResolver implements OnModuleInit {
  constructor(
    @Inject("ResponsaveisServiceClient")
    private readonly responsaveisServiceClient: ClientGrpcProxy,

    private readonly authService: AuthService,

    private readonly passwordUtils: PasswordUtils
  ) {}

  private responsaveisService: IResponsaveisService;

  async onModuleInit(): Promise<void> {
    this.responsaveisService = await this.responsaveisServiceClient.getService<
      IResponsaveisService
    >("ResponsavelsService");
  }

  @Mutation("cadastro")
  async signup(@Args("data") data: ResponsavelSignup): Promise<AuthPayload> {
    const { count } = await this.responsaveisService
      .count({
        where: JSON.stringify({ email: data.email }),
      })
      .toPromise();

    if (count >= 1) throw new Error("Email taken");

    const user: Responsaveis = await this.responsaveisService
      .create({
        ...data,
        password: await this.passwordUtils.hash(data.password),
      })
      .toPromise();

    const AcessToken = await this.authService.generateAccessToken(user);
    const refreshToken = await this.authService.generateRefreshToken(user);

    return {
      responsavel: user,
      accessToken: AcessToken,
      refreshToken: refreshToken,
    };
  }

  @Mutation("login")
  async login(
    @Context() context: any,
    @Args("data") data: ResponsavelSignin
  ): Promise<AuthPayload> {
    const { res } = context;

    const user: any = await this.responsaveisService
      .findOne({
        where: JSON.stringify({ email: data.email }),
      })
      .toPromise();

    if (isEmpty(user)) {
      return {
        errors: [
          {
            field: "Falha ao realizar login",
            message: ["Usuário não encontrado."],
          },
        ],
      };
    }

    const isSame: boolean = await this.passwordUtils.compare(
      data.password,
      user.password
    );

    if (!isSame) {
      return {
        errors: [
          {
            field: "Falha ao realizar login",
            message: ["Usuário ou senha inválidos."],
          },
        ],
      };
    }

    const AcessToken = await this.authService.generateAccessToken(user);
    const refreshToken = await this.authService.generateRefreshToken(user);

    res.cookie("access-token", AcessToken, {
      httpOnly: true,
      maxAge: 1.8e6,
    });

    res.cookie("refresh-token", refreshToken, {
      httpOnly: true,
      maxAge: 1.728e8,
    });

    return {
      responsavel: user,
      accessToken: AcessToken,
      refreshToken: refreshToken,
    };
  }

  @Mutation()
  @UseGuards(RefreshAuthGuard)
  async refreshToken(
    @Context() context: any,
    @CurrentUser() user: Responsaveis
  ): Promise<AuthPayload> {
    const { res } = context;

    const AcessToken = await this.authService.generateAccessToken(user);
    const refreshToken = await this.authService.generateRefreshToken(user);

    res.cookie("access-token", AcessToken, {
      httpOnly: true,
      maxAge: 1.8e6,
    });
    res.cookie("refresh-token", refreshToken, {
      httpOnly: true,
      maxAge: 1.728e8,
    });

    return {
      responsavel: user,
      accessToken: AcessToken,
      refreshToken: refreshToken,
    };
  }

  @Mutation()
  async logout(@Context() context: any): Promise<boolean> {
    const { res } = context;

    res.cookie("access-token", "", {
      httpOnly: true,
      maxAge: 0,
    });
    res.cookie("refresh-token", "", {
      httpOnly: true,
      maxAge: 0,
    });

    return true;
  }
}
