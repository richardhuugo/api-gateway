import { Injectable, Inject } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";

import { Responsaveis } from "../graphql/typings";

@Injectable()
export class AuthService {
  constructor(
    @Inject("JwtAccessTokenServiceAdmin")
    private readonly accessTokenService: JwtService,

    @Inject("JwtRefreshTokenServiceAdmin")
    private readonly refreshTokenService: JwtService
  ) {}

  async generateAccessToken(user: Responsaveis): Promise<string> {
    return this.accessTokenService.sign(
      {
        user: user.id,
      },
      {
        subject: user.id,
      }
    );
  }

  async generateRefreshToken(user: Responsaveis): Promise<string> {
    return this.refreshTokenService.sign(
      {
        user: user.email,
      },
      {
        subject: user.id,
      }
    );
  }
}
