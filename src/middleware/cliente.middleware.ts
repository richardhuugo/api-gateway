import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  OnModuleInit,
} from "@nestjs/common";
import { GqlExecutionContext } from "@nestjs/graphql";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { IEmpresasService } from "src/services/empresas/empresas.interface";

@Injectable()
export class ClienteMiddleware implements CanActivate, OnModuleInit {
  constructor(
    @Inject("EmpresasServiceClient")
    private readonly empresasServiceClient: ClientGrpcProxy
  ) {}

  private empresasService: IEmpresasService;

  async onModuleInit() {
    this.empresasService = await this.empresasServiceClient.getService<
      IEmpresasService
    >("EmpresasService");
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const ctx = GqlExecutionContext.create(context);
    const access_token = ctx.getContext().req.headers?.token_access || null;

    if (!access_token) return false;

    const empresa = await this.empresasService
      .findOne({
        where: JSON.stringify({ hash_integration: access_token }),
      })
      .toPromise();

    ctx.getContext().empresa = empresa;
    return await ctx.getContext().req;
  }
}
