import { join } from "path";
import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import {
  Transport,
  ClientProxyFactory,
  ClientGrpcProxy,
} from "@nestjs/microservices";

import { EspecialidadesTypeResolver } from "./especialidades-type.resolver";
import { EspecialidadesQueryResolver } from "./especialidades-query.resolver";
import { EspecialidadesMutationResolver } from "./especialidades-mutation.resolver";

import { UtilsModule } from "../../utils/utils.module";

@Module({
  imports: [ConfigModule, UtilsModule],
  providers: [
    EspecialidadesTypeResolver,
    EspecialidadesQueryResolver,
    EspecialidadesMutationResolver,
    {
      provide: "EspecialidadesServiceClient",
      useFactory: (configService: ConfigService): ClientGrpcProxy => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            url: "192.168.0.14:500567",
            package: "Especialidade",
            protoPath: join(__dirname, "../../_proto/especialidades.proto"),
            loader: {
              keepCase: true,
              enums: String,
              oneofs: true,
              arrays: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: ["EspecialidadesServiceClient"],
})
export class EspecialidadesModule {}
