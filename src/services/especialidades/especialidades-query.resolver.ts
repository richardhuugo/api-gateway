import { Inject, OnModuleInit } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Query, Resolver } from "@nestjs/graphql";

import { IEspecialidadesService } from "./especialidades.interface";
import { EspecialidadesConnection } from "src/graphql/typings";

@Resolver("Especialidades")
export class EspecialidadesQueryResolver implements OnModuleInit {
  constructor(
    @Inject("EspecialidadesServiceClient")
    private readonly EspecialidadesServiceClient: ClientGrpcProxy
  ) {}

  private especialidadesService: IEspecialidadesService;

  onModuleInit(): void {
    this.especialidadesService = this.EspecialidadesServiceClient.getService<
      IEspecialidadesService
    >("EspecialidadesService");
  }

  @Query("especialidades")
  // @UseGuards(GqlAuthGuard)
  async getEspecialidades(): Promise<EspecialidadesConnection> {
    return this.especialidadesService.find({}).toPromise();
  }
}
