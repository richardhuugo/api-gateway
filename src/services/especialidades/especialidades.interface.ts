import { Observable } from "rxjs";
import { Metadata } from "grpc";

import { IId, IQuery, ICount } from "../../commons/commons.interface";

import { EspecialidadesDto } from "./especialidades.dto";

interface UpdateEspecialidadeInput {
  id: string;
  data: EspecialidadesDto;
}

export interface IEspecialidadesService {
  find(query: IQuery, metadata?: Metadata);
  findById(id: IId, metadata?: Metadata);
  findOne(query: IQuery, metadata?: Metadata);
  count(query: IQuery, metadata?: Metadata): Observable<ICount>;
  create(input: EspecialidadesDto, metadata?: Metadata);
  update(input: UpdateEspecialidadeInput);
  destroy(query: IQuery, metadata?: Metadata): Observable<ICount>;
}
