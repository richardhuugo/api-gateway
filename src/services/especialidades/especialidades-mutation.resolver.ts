import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Resolver, Args, Mutation } from "@nestjs/graphql";

import { IEspecialidadesService } from "./especialidades.interface";
import { EspecialidadesInput, Especialidades } from "../../graphql/typings";

@Resolver()
export class EspecialidadesMutationResolver implements OnModuleInit {
  constructor(
    @Inject("EspecialidadesServiceClient")
    private readonly EspecialidadesServiceClient: ClientGrpcProxy
  ) {}

  private especialidadesService: IEspecialidadesService;

  onModuleInit(): void {
    this.especialidadesService = this.EspecialidadesServiceClient.getService<
      IEspecialidadesService
    >("EspecialidadesService");
  }

  @Mutation("createEspecialidade")
  async createTipoDocumento(
    @Args("data") data: EspecialidadesInput
  ): Promise<Especialidades> {
    return await this.especialidadesService
      .create({
        ...data,
      })
      .toPromise();
  }
}
