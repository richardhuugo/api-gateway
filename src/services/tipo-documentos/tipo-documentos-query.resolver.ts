import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Query, Resolver, Args } from "@nestjs/graphql";

import { isEmpty, merge } from "lodash";
import { PinoLogger } from "nestjs-pino";

import { ITipoDocumentosService } from "./tipo-documentos.interface";
import { QueryUtils } from "../../utils/query.utils";
import { TipoDocumentos, TipoDocumentosConnection } from "src/graphql/typings";
// import { GqlAuthGuard } from "../../../auth/gql-auth.guard";
// import { CurrentUser } from "../../auth/tipoDocumento.decorator";

@Resolver("TipoDocumentos")
export class TipoDocumentosQueryResolver implements OnModuleInit {
  constructor(
    @Inject("TipoDocumentosServiceClient")
    private readonly TipoDocumentosServiceClient: ClientGrpcProxy,

    private readonly queryUtils: QueryUtils // private readonly logger: PinoLogger
  ) {
    // logger.setContext(TipoDocumentosQueryResolver.name);
  }

  private TipoDocumentosService: ITipoDocumentosService;

  onModuleInit(): void {
    this.TipoDocumentosService = this.TipoDocumentosServiceClient.getService<
      ITipoDocumentosService
    >("TipoDocumentosService");
  }

  @Query("tipoDocumentos")
  // @UseGuards(GqlAuthGuard)
  async getTipoDocumentos(
    @Args("q") q: string,
    @Args("first") first: number,
    @Args("last") last: number,
    @Args("before") before: string,
    @Args("after") after: string,
    @Args("filterBy") filterBy: any,
    @Args("orderBy") orderBy: string
  ): Promise<TipoDocumentosConnection> {
    const query = { where: {} };

    if (!isEmpty(q)) merge(query, { where: { name: { _iLike: q } } });

    merge(
      query,
      await this.queryUtils.buildQuery(
        filterBy,
        orderBy,
        first,
        last,
        before,
        after
      )
    );
    return this.TipoDocumentosService.find({
      ...query,
      where: JSON.stringify(query.where),
    }).toPromise();
  }

  // @Query("tipoDocumento")
  // //  @UseGuards(GqlAuthGuard)
  // async getTipoDocumento(@Args("id") id: string): Promise<TipoDocumentos> {
  //   return this.TipoDocumentosService.findById({ id }).toPromise();
  // }

  // @Query("tipoDocumentoCount")
  // // @UseGuards(GqlAuthGuard)
  // async getTipoDocumentoCount(
  //   @Args("q") q: string,
  //   @Args("filterBy") filterBy: any
  // ): Promise<number> {
  //   const query = { where: {} };

  //   if (!isEmpty(q)) merge(query, { where: { name: { _iLike: q } } });

  //   merge(query, await this.queryUtils.getFilters(filterBy));

  //   const { count } = await this.TipoDocumentosService.count({
  //     ...query,
  //     where: JSON.stringify(query.where),
  //   }).toPromise();

  //   return count;
  // }
}
