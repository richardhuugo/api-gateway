import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Resolver, Args, Mutation } from "@nestjs/graphql";

import { PinoLogger } from "nestjs-pino";

import { ITipoDocumentosService } from "./tipo-documentos.interface";

import { PasswordUtils } from "../../utils/password.utils";
import { TipoDocumentoInput, TipoDocumentos } from "src/graphql/typings";
@Resolver()
export class TipoDocumentosMutationResolver implements OnModuleInit {
  constructor(
    @Inject("TipoDocumentosServiceClient")
    private readonly TipoDocumentosServiceClient: ClientGrpcProxy,

    private readonly passwordUtils: PasswordUtils // private readonly logger: PinoLogger
  ) {
    // logger.setContext(TipoDocumentosMutationResolver.name);
  }

  private TipoDocumentosService: ITipoDocumentosService;

  onModuleInit(): void {
    this.TipoDocumentosService = this.TipoDocumentosServiceClient.getService<
      ITipoDocumentosService
    >("TipoDocumentosService");
  }

  @Mutation("createTipoDocumento")
  // @UseGuards(GqlAuthGuard)
  async createTipoDocumento(
    @Args("data") data: TipoDocumentoInput
  ): Promise<any> {
    console.log(data);
    // this.logger.info("CommentsController#count.result %o", data);

    const tipoDocumento: TipoDocumentos = await this.TipoDocumentosService.create(
      {
        ...data,
      }
    ).toPromise();

    return {
      tipoDocumento,
    };
  }

  // @Mutation()
  // @UseGuards(GqlAuthGuard)
  // async updateEmail(
  //   @CurrentUser() user: any,
  //   @Args("data") data: UpdateEmailInput
  // ): Promise<UserPayload> {
  //   const { count } = await this.TipoDocumentosService.count({
  //     where: JSON.stringify({ email: data.email }),
  //   }).toPromise();

  //   if (count >= 1) throw new Error("Email taken");

  //   const isSame: boolean = await this.passwordUtils.compare(
  //     data.currentPassword,
  //     user.password
  //   );

  //   if (!isSame)
  //     throw new Error(
  //       "Error updating email. Kindly check the email or password provided"
  //     );

  //   const updatedUser: User = await this.TipoDocumentosService.update({
  //     id: user.id,
  //     data: {
  //       ...data,
  //     },
  //   }).toPromise();

  //   return { user: updatedUser };
  // }

  //   @Mutation()
  //   @UseGuards(GqlAuthGuard)
  //   async updatePassword(
  //     @CurrentUser() user: any,
  //     @Args("data") data: UpdatePasswordInput
  //   ): Promise<UserPayload> {
  //     const isSame: boolean = await this.passwordUtils.compare(
  //       data.currentPassword,
  //       user.password
  //     );
  //     const isConfirmed: boolean = data.newPassword === data.confirmPassword;

  //     if (!isSame || !isConfirmed) {
  //       throw new Error("Error updating password. Kindly check your passwords.");
  //     }

  //     const password: string = await this.passwordUtils.hash(data.newPassword);

  //     const updatedUser: any = await this.TipoDocumentosService.update({
  //       id: user.id,
  //       data: {
  //         password,
  //       },
  //     });

  //     return { user: updatedUser };
  //   }

  //   @Mutation()
  //   @UseGuards(GqlAuthGuard)
  //   async deleteAccount(
  //     @CurrentUser() user: User
  //   ): Promise<DeleteAccountPayload> {
  //     return this.TipoDocumentosService.destroy({
  //       where: JSON.stringify({
  //         id: user.id,
  //       }),
  //     }).toPromise();
  //   }
}
