import { Observable } from "rxjs";
import { Metadata } from "grpc";

import { IId, IQuery, ICount } from "../../commons/commons.interface";

import { TipoDocumentosDto } from "./tipo-documentos.dto";

interface UpdateTipoDocumentoInput {
  id: string;
  data: TipoDocumentosDto;
}

export interface ITipoDocumentosService {
  find(query: IQuery, metadata?: Metadata);
  findById(id: IId, metadata?: Metadata);
  findOne(query: IQuery, metadata?: Metadata);
  count(query: IQuery, metadata?: Metadata): Observable<ICount>;
  create(input: TipoDocumentosDto, metadata?: Metadata);
  update(input: UpdateTipoDocumentoInput);
  destroy(query: IQuery, metadata?: Metadata): Observable<ICount>;
}
