import { join } from "path";
import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import {
  Transport,
  ClientsModule,
  ClientProxyFactory,
  ClientGrpcProxy,
} from "@nestjs/microservices";

import { TipoDocumentosTypeResolver } from "./tipo-documentos-type.resolver";
import { TipoDocumentosQueryResolver } from "./tipo-documentos-query.resolver";
import { TipoDocumentosMutationResolver } from "./tipo-documentos-mutation.resolver";

import { UtilsModule } from "../../utils/utils.module";

@Module({
  imports: [ConfigModule, UtilsModule],
  providers: [
    TipoDocumentosTypeResolver,
    TipoDocumentosQueryResolver,
    TipoDocumentosMutationResolver,
    {
      provide: "TipoDocumentosServiceClient",
      useFactory: (configService: ConfigService): ClientGrpcProxy => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            url: "192.168.0.14:50053",
            package: "TipoDocumento",
            protoPath: join(__dirname, "../../_proto/tipo-documento.proto"),
            loader: {
              keepCase: true,
              enums: String,
              oneofs: true,
              arrays: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: ["TipoDocumentosServiceClient"],
})
export class TipoDocumentosModule {}
