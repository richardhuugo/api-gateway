import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Resolver, Args, Mutation } from "@nestjs/graphql";
import { Responsaveis, ResponsaveisPayload } from "src/graphql/typings";

import { ICount } from "src/commons/commons.interface";
import { merge } from "lodash";

import { ResponsaveisDto } from "./responsaveis.dto";
import { IResponsaveisService } from "./responsaveis.interface";

@Resolver()
export class ResponsaveisMutationResolver implements OnModuleInit {
  constructor(
    @Inject("ResponsaveisServiceClient")
    private readonly responsaveisServiceClient: ClientGrpcProxy
  ) {}

  private responsaveisService: IResponsaveisService;

  async onModuleInit(): Promise<void> {
    this.responsaveisService = await this.responsaveisServiceClient.getService<
      IResponsaveisService
    >("ResponsavelsService");
  }

  // /**
  //  * Função para criação de responsaveis
  //  */
  // @Mutation("createAdmin")
  // async createAdmin(
  //   @Args("data") data: ResponsaveisDto
  // ): Promise<ResponsaveisPayload> {
  //   // verificar tipo de documento
  //   // verifica se o documento está cadastrado
  //   const query = { where: {} };

  //   merge(query, {
  //     where: {
  //       email: { _iLike: data.email },
  //     },
  //   });

  //   const responsavel: ICount = await this.responsaveisService
  //     .count({
  //       where: JSON.stringify({
  //         email: { _iLike: data.email },
  //       }),
  //     })
  //     .toPromise();

  //   if (responsavel.count === 0) {
  //     return {
  //       errors: [
  //         {
  //           field: "Falha o registrar",
  //           message: ["O Email informado já se encontra cadastrado."],
  //         },
  //       ],
  //     };
  //   }
  //   const clienteCreatedCheck = await this.responsaveisService
  //     .create({
  //       ...data,
  //     })
  //     .toPromise();

  //   return {
  //     responsavel: clienteCreatedCheck,
  //   };
  // }
}
