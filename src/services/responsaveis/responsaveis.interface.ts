import { Observable } from "rxjs";
import { Metadata } from "grpc";

import { ICount, IId, IQuery } from "../../commons/commons.interface";

import { ResponsaveisDto } from "./responsaveis.dto";

interface UpdateUsuarioInput {
  id: string;
  data: ResponsaveisDto;
}

export interface IResponsaveisService {
  find(query: IQuery, metadata?: Metadata);
  findById(id: IId, metadata?: Metadata);
  count(query: IQuery, metadata?: Metadata): Observable<ICount>;
  findOne(query: IQuery, metadata?: Metadata);
  create(input: ResponsaveisDto, metadata?: Metadata);
  update(input: UpdateUsuarioInput);
}
