import { Field } from "@nestjs/graphql";
import { IsEmail, IsOptional, IsUUID } from "class-validator";

export class ResponsaveisDto {
  @Field()
  readonly nome_completo: string;

  @Field()
  @IsEmail()
  readonly email: string;

  @Field()
  readonly password?: string;

  @Field()
  readonly celular?: string;

  @Field()
  @IsOptional()
  @IsUUID()
  readonly empresa_id?: string;
}
