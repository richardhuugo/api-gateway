import { join } from "path";
import { Module, forwardRef } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";

import {
  Transport,
  ClientProxyFactory,
  ClientGrpcProxy,
} from "@nestjs/microservices";

import { UtilsModule } from "../../utils/utils.module";
import { DocumentosModule } from "../documentos/documentos.module";
import { TipoDocumentosModule } from "../tipo-documentos/tipo-documentos.module";
import { EmpresasModule } from "../empresas/empresas.module";
import { ResponsaveisTypeResolver } from "./responsaveis-type.resolver";
import { ResponsaveisQueryResolver } from "./responsaveis-query.resolver";
import { ResponsaveisMutationResolver } from "./responsaveis-mutation.resolver";

@Module({
  imports: [
    ConfigModule,
    UtilsModule,
    forwardRef(() => DocumentosModule),
    forwardRef(() => TipoDocumentosModule),
    forwardRef(() => EmpresasModule),
  ],
  providers: [
    ResponsaveisTypeResolver,
    ResponsaveisQueryResolver,
    ResponsaveisMutationResolver,
    {
      provide: "ResponsaveisServiceClient",
      useFactory: (configService: ConfigService): ClientGrpcProxy => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            url: "192.168.0.14:50598",
            package: "Responsavel",
            protoPath: join(__dirname, "../../_proto/responsaveis.proto"),
            loader: {
              keepCase: true,
              enums: String,
              oneofs: true,
              arrays: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: ["ResponsaveisServiceClient"],
})
export class ResponsaveisModule {}
