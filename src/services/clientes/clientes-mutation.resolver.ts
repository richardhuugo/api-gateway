import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Resolver, Args, Mutation } from "@nestjs/graphql";

import { PasswordUtils } from "../../utils/password.utils";
import {
  Clientes,
  ClientesPayload,
  Documentos,
  Empresas,
} from "src/graphql/typings";

import { ITipoDocumentosService } from "../tipo-documentos/tipo-documentos.interface";
import { ICount } from "src/commons/commons.interface";
import { merge } from "lodash";
import { IDocumentosService } from "../documentos/documentos.interface";
import { ClienteMiddleware } from "../../middleware/cliente.middleware";
import { CurrentEmpresa } from "src/config/empresa.decorator";

import { ClientesDto } from "./clientes.dto";
import { IClientesService } from "./clientes.interface";

@Resolver()
export class ClientesMutationResolver implements OnModuleInit {
  constructor(
    @Inject("ClientesServiceClient")
    private readonly clientesServiceClient: ClientGrpcProxy,
    @Inject("TipoDocumentosServiceClient")
    private readonly tipoDocumentosServiceClient: ClientGrpcProxy,
    @Inject("DocumentosServiceClient")
    private readonly documentosServiceClient: ClientGrpcProxy,

    private readonly passwordUtils: PasswordUtils
  ) {}

  private clientesService: IClientesService;
  private documentosService: IDocumentosService;
  private tipoDocumentosService: ITipoDocumentosService;

  async onModuleInit(): Promise<void> {
    this.clientesService = await this.clientesServiceClient.getService<
      IClientesService
    >("ClientesService");
    this.documentosService = await this.documentosServiceClient.getService<
      IDocumentosService
    >("DocumentosService");

    this.tipoDocumentosService = await this.tipoDocumentosServiceClient.getService<
      ITipoDocumentosService
    >("TipoDocumentosService");
  }

  /**
   * Função para criação de clientes
   */
  @Mutation("createUsuario")
  @UseGuards(ClienteMiddleware)
  async createUsuario(
    @Args("data") data: ClientesDto,
    @CurrentEmpresa() empresa: Empresas
  ): Promise<ClientesPayload> {
    // verificar tipo de documento
    // verifica se o documento está cadastrado
    const query = { where: {} };

    merge(query, {
      where: {
        nr_documento: { _iLike: data.documento },
      },
    });

    const tipoDocumento: ICount = await this.tipoDocumentosService
      .count({
        where: JSON.stringify({
          id: data.tipo_documento_id,
        }),
      })
      .toPromise();

    if (tipoDocumento.count === 0) {
      return {
        errors: [
          {
            field: "Falha o registrar",
            message: [
              "Não conseguimos realizar o cadastro do usuário, pois o tipo de documento não foi encontrado.",
            ],
          },
        ],
      };
    }
    // cadastrar o documento do usuario

    const documentFound: ICount = await this.documentosService
      .count({
        where: JSON.stringify(query.where),
      })
      .toPromise();

    // verifica se o cliente já se encontra vinculado a empresa
    if (documentFound.count > 0) {
      const documentoUser: Documentos = await this.documentosService
        .findOne({
          where: JSON.stringify(query.where),
        })
        .toPromise();

      const checkCliente: Clientes = await this.clientesService
        .findOne({
          where: JSON.stringify({
            id: documentoUser.user_id,
            empresa_id: empresa.id,
          }),
        })
        .toPromise();

      if (checkCliente) {
        return {
          errors: [
            {
              field: "Falha o registrar",
              message: [
                "Identificamos que este cliente já se encontra vinculado a esta empresa",
              ],
            },
          ],
        };
      }

      const clienteCreatedCheck = await this.clientesService
        .create({
          ...data,
          empresa_id: empresa.id,
        })
        .toPromise();

      return {
        cliente: clienteCreatedCheck,
      };
    }

    // cadastrar dados do cliente vinculando-o a empresa ativa
    const createCliente: Clientes = await this.clientesService
      .create({
        ...data,
        empresa_id: empresa.id,
      })
      .toPromise();

    // criar documento
    await this.documentosService
      .create({
        nr_documento: data.documento,
        tipo_documento_id: data.tipo_documento_id,
        cliente_id: createCliente.id,
      })
      .toPromise();

    return {
      cliente: createCliente,
    };
  }
}
