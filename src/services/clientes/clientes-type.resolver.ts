import { Inject, OnModuleInit } from "@nestjs/common";
import {
  Resolver,
  Args,
  Parent,
  ResolveField,
  Root,
  Context,
  Info,
} from "@nestjs/graphql";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { isEmpty, merge } from "lodash";
import { Clientes, Documentos } from "src/graphql/typings";
import { IDocumentosService } from "../documentos/documentos.interface";

@Resolver("Clientes")
export class ClientesTypeResolver implements OnModuleInit {
  constructor(
    @Inject("DocumentosServiceClient")
    private readonly documentosServiceClient: ClientGrpcProxy
  ) {}

  private documentosService: IDocumentosService;

  async onModuleInit() {
    this.documentosService = await this.documentosServiceClient.getService<
      IDocumentosService
    >("DocumentosService");
  }

  @ResolveField("documento")
  async getDocumento(
    @Parent() cliente: Clientes,
    @Args("q") q: string
  ): Promise<Documentos> {
    return await this.documentosService
      .findOne({
        where: JSON.stringify({ cliente_id: cliente.id }),
      })
      .toPromise();
  }
}
