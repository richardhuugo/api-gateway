import { ArgsType, Field } from "@nestjs/graphql";
import { IsEmail, IsUUID } from "class-validator";

export class ClientesDto {
  @Field()
  readonly nome_completo: string;

  @Field()
  readonly dt_nascimento: string;

  @Field()
  readonly sexo?: string;

  @Field()
  @IsEmail()
  readonly email?: string;

  @Field()
  readonly celular?: string;

  @Field()
  readonly telefone?: string;

  @Field()
  readonly documento?: string;

  @Field()
  @IsUUID()
  readonly tipo_documento_id?: string;

  readonly empresa_id?: string;
}
