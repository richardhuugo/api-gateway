import { Observable } from "rxjs";
import { Metadata } from "grpc";

import { IId, IQuery } from "../../commons/commons.interface";

import { ClientesDto } from "./clientes.dto";

interface UpdateUsuarioInput {
  id: string;
  data: ClientesDto;
}

export interface IClientesService {
  find(query: IQuery, metadata?: Metadata);
  findById(id: IId, metadata?: Metadata);
  findOne(query: IQuery, metadata?: Metadata);
  create(input: ClientesDto, metadata?: Metadata);
  update(input: UpdateUsuarioInput);
}
