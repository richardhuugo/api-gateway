import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Query, Resolver, Args } from "@nestjs/graphql";

import { IClientesService } from "./clientes.interface";
import { QueryUtils } from "../../utils/query.utils";
import { ClientesConnection, Empresas } from "src/graphql/typings";
import { CurrentEmpresa } from "../../config/empresa.decorator";
import { ClienteMiddleware } from "../../middleware/cliente.middleware";

@Resolver("Clientes")
export class ClientesQueryResolver implements OnModuleInit {
  constructor(
    @Inject("ClientesServiceClient")
    private readonly clientesServiceClient: ClientGrpcProxy,
    private readonly queryUtils: QueryUtils
  ) {}

  private clientesService: IClientesService;

  async onModuleInit() {
    this.clientesService = await this.clientesServiceClient.getService<
      IClientesService
    >("ClientesService");
  }

  @Query("clientes")
  @UseGuards(ClienteMiddleware)
  async getClientes(
    @Args("q") q: string,
    @Args("first") first: number,
    @Args("last") last: number,
    @Args("before") before: string,
    @Args("after") after: string,
    @Args("filterBy") filterBy: any,
    @Args("orderBy") orderBy: string,
    @CurrentEmpresa() empresa: Empresas
  ): Promise<ClientesConnection> {
    return this.clientesService
      .find({
        where: JSON.stringify({ empresa_id: empresa.id }),
      })
      .toPromise();
  }
}
