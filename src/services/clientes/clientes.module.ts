import { join } from "path";
import { Module, forwardRef } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";

import {
  Transport,
  ClientProxyFactory,
  ClientGrpcProxy,
} from "@nestjs/microservices";

import { UtilsModule } from "../../utils/utils.module";
import { DocumentosModule } from "../documentos/documentos.module";
import { TipoDocumentosModule } from "../tipo-documentos/tipo-documentos.module";
import { EmpresasModule } from "../empresas/empresas.module";
import { ClientesTypeResolver } from "./clientes-type.resolver";
import { ClientesQueryResolver } from "./clientes-query.resolver";
import { ClientesMutationResolver } from "./clientes-mutation.resolver";

@Module({
  imports: [
    ConfigModule,
    UtilsModule,
    forwardRef(() => DocumentosModule),
    forwardRef(() => TipoDocumentosModule),
    forwardRef(() => EmpresasModule),
  ],
  providers: [
    ClientesTypeResolver,
    ClientesQueryResolver,
    ClientesMutationResolver,
    {
      provide: "ClientesServiceClient",
      useFactory: (configService: ConfigService): ClientGrpcProxy => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            url: "192.168.0.14:50055",
            package: "Cliente",
            protoPath: join(__dirname, "../../_proto/clientes.proto"),
            loader: {
              keepCase: true,
              enums: String,
              oneofs: true,
              arrays: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: ["ClientesServiceClient"],
})
export class ClientesModule {}
