export class EmpresasDto {
  readonly nm_fantasia?: string;
  readonly razao_social?: string;
  readonly email?: string;
  readonly telefone?: string;
  readonly documento?: string;
  readonly documento_id?: string;
}
