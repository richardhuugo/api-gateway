import { join } from "path";
import { Module, forwardRef } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";

import {
  Transport,
  ClientsModule,
  ClientProxyFactory,
  ClientGrpcProxy,
} from "@nestjs/microservices";

import { EmpresasTypeResolver } from "./empresas-type.resolver";
import { EmpresasQueryResolver } from "./empresas-query.resolver";
import { EmpresasMutationResolver } from "./empresas-mutation.resolver";

import { UtilsModule } from "../../utils/utils.module";
import { DocumentosModule } from "../documentos/documentos.module";
import { TipoDocumentosModule } from "../tipo-documentos/tipo-documentos.module";
import { SearchModule } from "src/elasticsearch/search.module";

@Module({
  imports: [
    ConfigModule,
    UtilsModule,
    forwardRef(() => DocumentosModule),
    forwardRef(() => TipoDocumentosModule),
    SearchModule,
    // ClientsModule.register([
    //   {
    //     name: "EmpresasServiceClient",
    //     transport: Transport.GRPC,
    //     options: {
    //       url: "192.168.0.14:50060",
    //       package: "Empresa",
    //       protoPath: join(__dirname, "../../_proto/empresa.proto"),
    //       loader: {
    //         keepCase: true,
    //         enums: String,
    //         oneofs: true,
    //         arrays: true,
    //       },
    //     },
    //   },
    // ]),
  ],
  providers: [
    EmpresasTypeResolver,
    EmpresasQueryResolver,
    EmpresasMutationResolver,
    {
      provide: "EmpresasServiceClient",
      useFactory: (configService: ConfigService): ClientGrpcProxy => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            url: "192.168.0.14:50060",
            package: "Empresa",
            protoPath: join(__dirname, "../../_proto/empresa.proto"),
            loader: {
              keepCase: true,
              enums: String,
              oneofs: true,
              arrays: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: ["EmpresasServiceClient"],
})
export class EmpresasModule {}
