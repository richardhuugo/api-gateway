import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Query, Resolver, Args } from "@nestjs/graphql";

import { isEmpty, merge } from "lodash";

import { IEmpresasService } from "./empresas.interface";
import { EmpresasConnection } from "src/graphql/typings";
import { QueryUtils } from "../../utils/query.utils";
import SearchService from "src/elasticsearch/search.service";
// import { ClienteMiddleware } from "src/middleware/cliente.middleware";

@Resolver("Empresas")
export class EmpresasQueryResolver implements OnModuleInit {
  constructor(
    @Inject("EmpresasServiceClient")
    private readonly empresasServiceClient: ClientGrpcProxy,

    private readonly queryUtils: QueryUtils,
    private readonly elasticSearchService: SearchService
  ) {}

  private empresasService: IEmpresasService;

  onModuleInit(): void {
    this.empresasService = this.empresasServiceClient.getService<
      IEmpresasService
    >("EmpresasService");
  }

  @Query("empresas")
  // @UseGuards(ClienteMiddleware)
  async getEmpresas(
    @Args("q") q: string,
    @Args("first") first: number,
    @Args("page") page: number,
    @Args("last") last: number,
    @Args("before") before: string,
    @Args("after") after: string,
    @Args("filterBy") filterBy: any,
    @Args("orderBy") orderBy: string
  ): Promise<EmpresasConnection> {
    const query = { where: {} };

    if (!isEmpty(q)) {
      const results = await this.elasticSearchService.search(q, "empresas", [
        "documento",
        "nm_fantasia",
        "razao_social",
      ]);
      const ids = results.map((result: any) => result.id);

      merge(query, { where: { id: ids } });
    }

    merge(
      query,
      await this.queryUtils.buildQuery(
        filterBy,
        orderBy,
        first,
        last,
        before,
        after
      )
    );

    return this.empresasService
      .find({
        ...query,
        page,
        where: JSON.stringify(query.where),
      })
      .toPromise();
  }
}
