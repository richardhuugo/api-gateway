import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Resolver, Args, Mutation } from "@nestjs/graphql";
import { IEmpresasService } from "./empresas.interface";
import {
  DocumentoInput,
  Documentos,
  EmpresaInput,
  Empresas,
  EmpresasPayload,
} from "src/graphql/typings";
import { IDocumentosService } from "../documentos/documentos.interface";
import { merge } from "lodash";
import { ICount } from "src/commons/commons.interface";
import { ITipoDocumentosService } from "../tipo-documentos/tipo-documentos.interface";
import SearchService from "../../elasticsearch/search.service";

@Resolver()
export class EmpresasMutationResolver implements OnModuleInit {
  constructor(
    @Inject("DocumentosServiceClient")
    private readonly documentosServiceClient: ClientGrpcProxy,

    @Inject("TipoDocumentosServiceClient")
    private readonly tipoDocumentosServiceClient: ClientGrpcProxy,

    @Inject("EmpresasServiceClient")
    private readonly empresasServiceClient: ClientGrpcProxy,

    private readonly elasticSearchService: SearchService
  ) {}

  private empresasService: IEmpresasService;
  private documentosService: IDocumentosService;
  private tipoDocumentosService: ITipoDocumentosService;

  async onModuleInit() {
    this.empresasService = await this.empresasServiceClient.getService<
      IEmpresasService
    >("EmpresasService");
    this.documentosService = await this.documentosServiceClient.getService<
      IDocumentosService
    >("DocumentosService");
    this.tipoDocumentosService = await this.tipoDocumentosServiceClient.getService<
      ITipoDocumentosService
    >("TipoDocumentosService");
  }

  @Mutation("createEmpresa")
  async createEmpresa(
    @Args("data") data: EmpresaInput
  ): Promise<EmpresasPayload> {
    // verifica se o documento está cadastrado
    const query = { where: {} };

    merge(query, { where: { nr_documento: { _iLike: data.documento } } });

    const tipoDocumento: ICount = await this.tipoDocumentosService
      .count({
        where: JSON.stringify({ id: data.tipo_documento_id }),
      })
      .toPromise();

    if (tipoDocumento.count === 0) {
      return {
        errors: [
          {
            field: "Falha o registrar",
            message: [
              "Não conseguimos realizar o cadastro da empresa, pois o tipo de documento não foi encontrado.",
            ],
          },
        ],
      };
    }

    const documentFound: ICount = await this.documentosService
      .count({
        where: JSON.stringify(query.where),
      })
      .toPromise();

    if (documentFound.count > 0) {
      return {
        errors: [
          {
            field: "Falha o registrar",
            message: [
              "Não conseguimos realizar o cadastro da empresa, pois o CNPJ já se encontra cadastrado",
            ],
          },
        ],
      };
    }

    // cria o documento
    const createDocumento: Documentos = await this.documentosService
      .create({
        nr_documento: data.documento,
        tipo_documento_id: data.tipo_documento_id,
      })
      .toPromise();

    // vincula o documento para criação da empresa
    const docResult: Empresas = await this.empresasService
      .create({
        ...data,
        documento_id: createDocumento.id,
      })
      .toPromise();

    const dataElastic = {
      ...docResult,
      documento: createDocumento.nr_documento,
    };

    this.elasticSearchService.indexSearch({
      index: "empresas",
      context: dataElastic,
    });

    return {
      empresa: docResult,
    };
  }
}
