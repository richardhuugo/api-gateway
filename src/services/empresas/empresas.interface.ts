import { Metadata } from "grpc";

import { IId, IQuery } from "../../commons/commons.interface";

import { EmpresasDto } from "./empresas.dto";

interface UpdateEmpresaInput {
  id: string;
  data: EmpresasDto;
}

export interface IEmpresasService {
  find(query: IQuery, metadata?: Metadata);
  findById(id: IId, metadata?: Metadata);
  findOne(query: IQuery, metadata?: Metadata);
  create(input: EmpresasDto, metadata?: Metadata);
  update(input: UpdateEmpresaInput);
}
