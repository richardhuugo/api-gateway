import { Inject, OnModuleInit } from "@nestjs/common";
import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Documentos } from "src/graphql/typings";
import { IDocumentosService } from "../documentos/documentos.interface";
import { EmpresasDto } from "./empresas.dto";
import { IEmpresasService } from "./empresas.interface";

@Resolver("Empresas")
export class EmpresasTypeResolver implements OnModuleInit {
  constructor(
    @Inject("DocumentosServiceClient")
    private readonly documentosServiceClient: ClientGrpcProxy,

    @Inject("EmpresasServiceClient")
    private readonly empresasServiceClient: ClientGrpcProxy
  ) {}

  private empresasService: IEmpresasService;
  private documentosService: IDocumentosService;

  onModuleInit(): void {
    this.empresasService = this.empresasServiceClient.getService<
      IEmpresasService
    >("EmpresasService");

    this.documentosService = this.documentosServiceClient.getService<
      IDocumentosService
    >("DocumentosService");
  }

  @ResolveField("documento")
  async getAuthor(@Parent() documento: EmpresasDto): Promise<Documentos> {
    console.log("called_here");

    const documentoResult: Documentos = await this.documentosService
      .findById({
        id: documento.documento_id,
      })
      .toPromise();
    console.log(documento);
    console.log(documentoResult);
    return documentoResult;
  }
}
