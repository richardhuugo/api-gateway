import { Inject, OnModuleInit } from "@nestjs/common";
import { Resolver } from "@nestjs/graphql";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { QueryUtils } from "../../utils/query.utils";
import { IDocumentosService } from "./documentos.interface";

@Resolver("Documentos")
export class DocumentosTypeResolver implements OnModuleInit {
  constructor(
    @Inject("DocumentosServiceClient")
    private readonly documentosServiceClient: ClientGrpcProxy,

    private readonly queryUtils: QueryUtils
  ) {}

  private tipoDocumentosService: IDocumentosService;

  onModuleInit(): void {
    this.tipoDocumentosService = this.documentosServiceClient.getService<
      IDocumentosService
    >("DocumentosService");
  }
}
