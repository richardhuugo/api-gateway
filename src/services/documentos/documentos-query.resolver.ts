import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Query, Resolver, Args } from "@nestjs/graphql";

import { isEmpty, merge } from "lodash";
import { IDocumentosService } from "./documentos.interface";
import { QueryUtils } from "../../utils/query.utils";
import { Documentos } from "src/graphql/typings";

@Resolver("Documentos")
export class DocumentosQueryResolver implements OnModuleInit {
  constructor(
    @Inject("DocumentosServiceClient")
    private readonly documentosServiceClient: ClientGrpcProxy,

    private readonly queryUtils: QueryUtils
  ) {}

  private documentosService: IDocumentosService;

  onModuleInit(): void {
    this.documentosService = this.documentosServiceClient.getService<
      IDocumentosService
    >("DocumentosService");
  }

  // @Query("documento")
  // // @UseGuards(GqlAuthGuard)
  // async getDocumento(
  //   @Args("q") q: string,
  //   @Args("first") first: number,
  //   @Args("last") last: number,
  //   @Args("before") before: string,
  //   @Args("after") after: string,
  //   @Args("filterBy") filterBy: any,
  //   @Args("orderBy") orderBy: string
  // ): Promise<Documentos> {
  //   const query = { where: {} };

  //   if (!isEmpty(q)) merge(query, { where: { nr_documento: { _iLike: q } } });

  //   merge(
  //     query,
  //     await this.queryUtils.buildQuery(
  //       filterBy,
  //       orderBy,
  //       first,
  //       last,
  //       before,
  //       after
  //     )
  //   );

  //   return this.documentosService
  //     .findOne({
  //       ...query,
  //       where: JSON.stringify(query.where),
  //     })
  //     .toPromise();
  // }
}
