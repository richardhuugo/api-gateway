import { Observable } from "rxjs";
import { Metadata } from "grpc";

import { ICount, IId, IQuery } from "../../commons/commons.interface";

import { DocumentosDto } from "./documentos.dto";
import { Documentos } from "src/graphql/typings";

export interface IDocumentosService {
  findOne(query: IQuery, metadata?: Metadata): Observable<Documentos>;
  findById(id: IId, metadata?: Metadata): Observable<Documentos>;
  count(query: IQuery, metadata?: Metadata): Observable<ICount>;
  create(input: DocumentosDto, metadata?: Metadata);
}
