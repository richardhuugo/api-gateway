import { join } from "path";
import { Module, forwardRef } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";

import {
  Transport,
  ClientsModule,
  ClientProxyFactory,
  ClientGrpcProxy,
} from "@nestjs/microservices";

import { DocumentosTypeResolver } from "./documentos-type.resolver";
import { DocumentosQueryResolver } from "./documentos-query.resolver";
import { DocumentosMutationResolver } from "./documentos-mutation.resolver";

import { UtilsModule } from "../../utils/utils.module";
import { TipoDocumentosModule } from "../tipo-documentos/tipo-documentos.module";

@Module({
  imports: [ConfigModule, UtilsModule, forwardRef(() => TipoDocumentosModule)],
  providers: [
    DocumentosTypeResolver,
    DocumentosQueryResolver,
    DocumentosMutationResolver,
    {
      provide: "DocumentosServiceClient",
      useFactory: (configService: ConfigService): ClientGrpcProxy => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            url: "192.168.0.14:50051",
            package: "Documento",
            protoPath: join(__dirname, "../../_proto/documento.proto"),
            loader: {
              keepCase: true,
              enums: String,
              oneofs: true,
              arrays: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: ["DocumentosServiceClient"],
})
export class DocumentosModule {}
