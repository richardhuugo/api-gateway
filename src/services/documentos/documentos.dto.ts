export class DocumentosDto {
  readonly nr_documento?: string;
  readonly tipo_documento_id?: string;
  readonly cliente_id?: string;
}
