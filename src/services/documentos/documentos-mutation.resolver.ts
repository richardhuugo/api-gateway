import { Inject, OnModuleInit, UseGuards } from "@nestjs/common";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { Resolver, Args, Mutation } from "@nestjs/graphql";

import { PinoLogger } from "nestjs-pino";

import { IDocumentosService } from "./documentos.interface";
// import {
//   User,
//   UserPayload,
//   UpdateProfileInput,
//   UpdateEmailInput,
//   UpdatePasswordInput,
//   DeleteAccountPayload,
// } from "../../graphql/typings";

import { PasswordUtils } from "../../utils/password.utils";
import {
  DocumentoInput,
  Documentos,
  DocumentosPayload,
} from "src/graphql/typings";
// import { GqlAuthGuard } from "../../auth/gql-auth.guard";
// import { CurrentUser } from "../../auth/user.decorator";

@Resolver()
export class DocumentosMutationResolver implements OnModuleInit {
  constructor(
    @Inject("DocumentosServiceClient")
    private readonly DocumentosServiceClient: ClientGrpcProxy,

    private readonly passwordUtils: PasswordUtils // private readonly logger: PinoLogger
  ) {
    // logger.setContext(DocumentosMutationResolver.name);
  }

  private DocumentosService: IDocumentosService;

  onModuleInit(): void {
    this.DocumentosService = this.DocumentosServiceClient.getService<
      IDocumentosService
    >("DocumentosService");
  }

  @Mutation("createDocumento")
  // @UseGuards(GqlAuthGuard)
  async createDocumento(
    @Args("data") data: DocumentoInput
  ): Promise<DocumentosPayload> {
    console.log(data);
    // this.logger.info("CommentsController#count.result %o", data);

    const docResult: Documentos = await this.DocumentosService.create({
      ...data,
    }).toPromise();

    return {
      documento: docResult,
    };
  }
}
